# AWS Step Functions and Lambda Integration for Processing S3 JSON Data

This README outlines the process to set up a workflow where AWS Step Functions orchestrates a Lambda function to process JSON data stored in an S3 bucket. The workflow fetches a JSON file from S3, parses it, and passes the data to a Lambda function for processing.


## 1. Setting Up the Lambda Function

### a. Create Lambda Function in Rust

Below is the Rust code for the Lambda function, designed to parse JSON data and output greetings for each name provided in the JSON array.

```rust
use lambda_runtime::{Error, LambdaEvent, Context, handler_fn};
use serde_json::{Value, json};
use serde::{Deserialize, Serialize};
use tokio;

#[derive(Deserialize)]
struct InputEvent {
    names: Vec<String>,
}

#[derive(Serialize)]
struct OutputEvent {
    messages: Vec<String>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(greet_names);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn greet_names(event: LambdaEvent<Value>, _: Context) -> Result<Value, Error> {
    let input: InputEvent = serde_json::from_value(event.payload)?;

    let messages: Vec<String> = input.names.iter()
        .map(|name| format!("Hello, {}!", name))
        .collect();

    let output = OutputEvent { messages };
    Ok(json!(output))
}
```

- **Deployment**: Deploy this function to AWS Lambda using the AWS CLI or Lambda management console. Ensure the execution role has appropriate permissions for S3 access and CloudWatch Logs.


## 2. Configuring AWS Step Functions

Set up a state machine to orchestrate the data flow from S3 to the Lambda function.

### Step Functions Definition

Create a JSON definition for the AWS Step Functions state machine:

```json
{
  "Comment": "Process existing JSON from S3 using Lambda",
  "StartAt": "FetchAndProcessData",
  "States": {
    "FetchAndProcessData": {
      "Type": "Task",
      "Resource": "arn:aws:states:::aws-sdk:s3:getObject",
      "Parameters": {
        "Bucket": "p4ind",
        "Key": "p4.json"
      },
      "Next": "ParseJSON"
    },
    "ParseJSON": {
      "Type": "Pass",
      "Parameters": {
        "data.$": "States.StringToJson($.Payload.Body)"
      },
      "Next": "InvokeLambdaFunction"
    },
    "InvokeLambdaFunction": {
      "Type": "Task",
      "Resource": "arn:aws:states:::lambda:invoke",
      "Parameters": {
        "FunctionName": "arn:aws:lambda:us-east-1:339712872070:function:ind4",
        "Payload.$": "$.ParsedData.data"
      },
      "End": true
    }
  },
  "TimeoutSeconds": 300
}
```

- **FetchAndProcessData**: Retrieves the JSON file from S3.
- **ParseJSON**: Converts the JSON string to a JSON object.
- **InvokeLambdaFunction**: Invokes the Lambda function with the JSON object as input.

### Deployment

Deploy this state machine using the AWS Management Console or AWS CLI, attaching the previously created IAM role for execution permissions.

### Demo
![viedo](Executions_ a560cf01-07ec-42b8-a912-fb340521b9dd _ Step Functions _ us-east-1 - Google Chrome 2024-04-12 02-38-50.mp4)
