# Use an official Rust image
FROM rust:latest as builder

# Install musl-tools for the musl-gcc compiler
RUN apt-get update
RUN apt-get install -y musl-tools
RUN rustup target add x86_64-unknown-linux-musl

# Create a folder for your app
WORKDIR /usr/src/myapp

# Copy your source tree
COPY ./ ./

# Build your application
RUN cargo build --release --target x86_64-unknown-linux-musl

# Create a new stage with a minimal image
# because we only need the binary executable
FROM alpine:latest
WORKDIR /root/
COPY --from=builder /usr/src/myapp/target/x86_64-unknown-linux-musl/release/rust_lambda .

# Set the CMD to your handler (could also be done as an ENTRYPOINT)
CMD ["./rust_lambda"]